# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_length = Hash.new(0)
  words = str.split
  words.each { |word| word_length[word] = word.length }
  word_length
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  sorted_ints = hash.sort_by { |_k, v| v }
  sorted_ints[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |key, value|
    older[key] = value
  end

  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letter_count = Hash.new(0)
  word.each_char { |ch| letter_count[ch] += 1 }
  letter_count
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hash = Hash.new(0)
  arr.each { |el| hash[el] += 1 }
  hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  count = {
    even: 0,
    odd: 0
  }
  numbers.each do |num|
    count[:even] += 1 if num.even?
    count[:odd] += 1 if num.odd?
  end

  count
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowel_count = Hash.new(0)
  string.downcase.each_char do |ch|
    vowel_count[ch] += 1 if "aeiou".include?(ch)
  end
  sorted_vowels = vowel_count.sort_by { |_key, value| value }
  sorted_vowels[-1][0]
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  birthdays = students.select { |_name, birthday| birthday > 6 }
  kids = birthdays.keys
  combos = []
  kids.each_with_index do |name, idx|
    kids[(idx + 1)..kids.length].each do |name2|
      combos << [name, name2]
    end
  end

  combos
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  specimen_count = Hash.new(0)
  specimens.each { |el| specimen_count[el] += 1 }

  num_species = specimen_count.length
  smallest_pop = specimen_count.values.min
  largest_pop = specimen_count.values.max

  num_species**2 * smallest_pop / largest_pop
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  only_letters = normal_sign.delete("^A-Za-z")
  only_vandal_letters = vandalized_sign.delete("^A-Za-z")
  normal_letters = character_count(only_letters)
  vandal_letters = character_count(only_vandal_letters)

  vandal_letters.all? do |letter, num|
    num <= normal_letters[letter]
  end
end

def character_count(str)
  letter_count = Hash.new(0)
  str.each_char do |ch|
    next if ch == " "
    letter_count[ch.downcase] += 1
  end

  letter_count
end
